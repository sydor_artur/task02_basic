package com.epam.train;

import java.util.Scanner;

/**
 * This class take a range of numbers, print odd numbers in increasing order and
 * even numbers in decreasing order.
 * Also it builds set of fibonacci numbers
 *
 * @author artur
 */

public class FibonacciNumber {
    /**
     * This variable is the begin point of number range.
     */
    private int beginOfRange;
    /**
     * This variable is the end point of number range.
     */
    private int endOfRange;
    /**
     * This variable sets size of fibonacci numbers set.
     */
    private int rangeOfFibonacciSet;
    /**
     * This variable take user data from console.
     */
    private Scanner scan = new Scanner(System.in);
    /**
     * This variable save sum of even numbers.
     */
    private int sumOfEven;
    /**
     * This variable save sum of odd numbers.
     */
    private int sumOfOdd;
    /**
     * This variable save set of fibonacci numbers.
     */
    private int[] fibonacciSet;

    /**
     * This method is utility, that help to know if the number is even of not.
     *
     * @param number checks for parity
     * @return true if number is even
     */
    private boolean isEven(final int number) {
        return number % 2 == 0;
    }

    /**
     * This method take from user begin and end points of number range.
     */
    public final void takeUserInput() {
        System.out.print("Enter begin of range: ");
        beginOfRange = scan.nextInt();
        System.out.print("Enter end of range: ");
        endOfRange = scan.nextInt();
    }

    /**
     * Method prints only odd number in increasing.
     */
    public final void printOddNumbers() {
        System.out.println("Odd numbers");
        if (!isEven(beginOfRange)) {
            for (int i = beginOfRange; i <= endOfRange; i += 2) {
                System.out.print(i + ", ");
            }
        } else {
            //add 1 to beginOfRange to start from odd number
            for (int i = beginOfRange + 1; i <= endOfRange; i += 2) {
                System.out.print(i + ", ");
            }
        }
        System.out.println("");
    }

    /**
     * Method prints only even numbers in decreasing.
     */
    public final void printEvenNumbers() {
        System.out.println("Even numbers");
        if (isEven(endOfRange)) {
            for (int i = endOfRange; i >= beginOfRange; i -= 2) {
                System.out.print(i + ", ");
            }
        } else {
            //substruct 1 to beginOfRange to start from even number
            for (int i = endOfRange - 1; i >= beginOfRange; i -= 2) {
                System.out.print(i + ", ");
            }
        }
        System.out.println("");
    }

    /**
     * This method calculate and print separately
     * sum of even numbers and then odd.
     */
    public final void printSumOfOddAndEven() {
        for (int i = beginOfRange; i <= endOfRange; i++) {
            if (isEven(i)) {
                sumOfEven += i;
            } else {
                sumOfOdd += i;
            }
        }
        System.out.print("Sum of even: " + sumOfEven + "\n");
        System.out.print("Sum of odd: " + sumOfOdd + "\n");
    }

    /**
     * Method build fibonacci sequence.
     * Then find the find F1 and F2:
     * F1 is the biggest odd number
     * F2 is the biggest even number.
     */
    public final void buildFibonacciNumbers() {
        System.out.println("Enter size of Fibonacci set: ");
        rangeOfFibonacciSet = scan.nextInt();
        if (rangeOfFibonacciSet <= 0) {
            System.out.println("Size of set cannot be negative");
            return;
        }
        fibonacciSet = new int[rangeOfFibonacciSet];

        System.out.println("F1 is the biggest odd number");

        System.out.println("F2 is the biggest even number");

        fibonacciSet[0] = 0;
        fibonacciSet[1] = 1;
        for (int i = 0, j = 1, k = 2; k < rangeOfFibonacciSet; i++, j++, k++) {
            fibonacciSet[k] = fibonacciSet[i] + fibonacciSet[j];
        }

        for (int fibonacci : fibonacciSet) {
            System.out.print(fibonacci + ", ");
        }
        System.out.println("");

        System.out.println("F1 is the biggest odd number");
        for (int i = rangeOfFibonacciSet - 1; i >= 0; i--) {
            if (!isEven(fibonacciSet[i])) {
                System.out.println(fibonacciSet[i]);
                break;
            }
        }
        System.out.println("F2 is the biggest even number");
        for (int i = rangeOfFibonacciSet - 1; i >= 0; i--) {
            if (isEven(fibonacciSet[i])) {
                System.out.println(fibonacciSet[i]);
                break;
            }
        }
    }

    /**
     * Method prints percentage of even and odd numbers.
     */
    public final void printPercentageOfOddAndEvenFibonacci() {
        final int oneHundredPercents = 100;
        int percentageOfEven;
        int percentageOfOdd;
        int counterOfEven = 0;

        for (int i = 0; i < rangeOfFibonacciSet; i++) {
            if (isEven(fibonacciSet[i])) {
                counterOfEven++;
            }
        }

        //calculate percentage of even fibonacci numbers
        percentageOfEven = (int) ((double) counterOfEven
                / rangeOfFibonacciSet * oneHundredPercents);
        //percentageOfOdd = 100% - percentageOfEven
        percentageOfOdd = oneHundredPercents - percentageOfEven;

        System.out.print("Percentage of even: " + percentageOfEven + "%\n");
        System.out.print("Percentage of odd: " + percentageOfOdd + "%\n");
    }
}
