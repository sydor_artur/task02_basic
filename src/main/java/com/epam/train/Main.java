package com.epam.train;

/**
 * Start point of application.
 */

public final class Main {
    /**
     * Private constructor for utility class.
     */
    private Main() {

    }

    /**
     * Method run program and test object "fibonaccinumber".
     *
     * @param args is an array of string
     */
    public static void main(final String[] args) {
        FibonacciNumber fibonacciNumber = new FibonacciNumber();
        fibonacciNumber.takeUserInput();
        fibonacciNumber.printEvenNumbers();
        fibonacciNumber.printOddNumbers();
        fibonacciNumber.printSumOfOddAndEven();
        fibonacciNumber.buildFibonacciNumbers();
        fibonacciNumber.printPercentageOfOddAndEvenFibonacci();
    }
}
